package de.mo.TwitterMap.model;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Base64;
import java.util.List;
import java.util.concurrent.Future;
import java.util.function.Consumer;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.ning.http.client.AsyncCompletionHandler;
import com.ning.http.client.AsyncHttpClient;
import com.ning.http.client.Response;

import elemental.json.Json;
import elemental.json.JsonArray;
import elemental.json.JsonObject;

public class TwitterService {
	
	private static final String key = "K5cBzkAaaMN5ulPLhuIyBqAP3:JX903pzxsltAkbZFLQuGGxvmtwXehLLIz3EV5MKOXz0VOiELpH";
			
	public TwitterService() {
	}

	/**
	 * get auth bear token as future
	 * @return
	 */
	public static Future<String> getToken() {
		byte[]   keyEncoded = Base64.getEncoder().encode(key.getBytes());
		String key = new String(keyEncoded);
		
		return new AsyncHttpClient().preparePost("https://api.twitter.com/oauth2/token")
			.addHeader("Authorization", "Basic " + key)
			.addHeader("Content-Type", "application/x-www-form-urlencoded;charset=UTF-8")
			.addFormParam("grant_type", "client_credentials")
			.execute(new AsyncCompletionHandler<String>(){

				@Override
				public String onCompleted(Response res) throws Exception {
					Gson g = new Gson();
					Type collectionType = new TypeToken<TokenResult>(){}.getType();
					TokenResult token = g.<TokenResult>fromJson(res.getResponseBody(), collectionType);
					return token.getAccess_token();
				}
				
			});
	}
	
	/**
	 * get auth bear token asynchronously
	 * @param callback
	 */
	public static void getToken(Consumer<String> callback) {
		byte[]   keyEncoded = Base64.getEncoder().encode(key.getBytes());
		String key = new String(keyEncoded);
		
		new AsyncHttpClient().preparePost("https://api.twitter.com/oauth2/token")
			.addHeader("Authorization", "Basic " + key)
			.addHeader("Content-Type", "application/x-www-form-urlencoded;charset=UTF-8")
			.addFormParam("grant_type", "client_credentials")
			.execute(new AsyncCompletionHandler<String>(){

				@Override
				public String onCompleted(Response res) throws Exception {
					Gson g = new Gson();
					Type collectionType = new TypeToken<TokenResult>(){}.getType();
					TokenResult token = g.<TokenResult>fromJson(res.getResponseBody(), collectionType);
					callback.accept(token.getAccess_token());
					return(token.getAccess_token());
				}
				
			});
	}
	
	/**
	 * get polular tweets as future
	 */
	public static Future<List<Tweet>> searchTweets(String token, String search) {
		String url = "https://api.twitter.com/1.1/search/tweets.json";
		return new AsyncHttpClient().prepareGet(url)
				.addHeader("Authorization", "Bearer " + token)
				.addHeader("User-Agent", "TwitterMapReactive")
				.addQueryParam("q", search)
				.addQueryParam("result_type", "popular")
				.execute(new AsyncCompletionHandler<List<Tweet>>(){

					@Override
					public List<Tweet> onCompleted(Response res) throws Exception {
						List<Tweet> tweets = new ArrayList<Tweet>();
						JsonArray statuses = Json.parse(res.getResponseBody()).getArray("statuses");
						for(int i = 0; i < statuses.length(); i++) {
							Tweet tweet = parseTweet(statuses.getObject(i));
							tweets.add(tweet);
						}
						return tweets;
					}
				});
	}
	
	/**
	 * get polular tweets asynchronously
	 */
	public static void searchTweets(String token, String search, Consumer<List<Tweet>> callback) {
		String url = "https://api.twitter.com/1.1/search/tweets.json";
		new AsyncHttpClient().prepareGet(url)
				.addHeader("Authorization", "Bearer " + token)
				.addHeader("User-Agent", "TwitterMapReactive")
				.addQueryParam("q", search)
				.addQueryParam("result_type", "popular")
				.execute(new AsyncCompletionHandler<List<Tweet>>(){

					@Override
					public List<Tweet> onCompleted(Response res) throws Exception {
						List<Tweet> tweets = new ArrayList<Tweet>();
						JsonArray statuses = Json.parse(res.getResponseBody()).getArray("statuses");
						for(int i = 0; i < statuses.length(); i++) {
							Tweet tweet = parseTweet(statuses.getObject(i));
							tweets.add(tweet);
						}
						callback.accept(tweets);
						return tweets;
					}
				});
	}
	
	private static Tweet parseTweet(JsonObject tweetJson) {
		Tweet tweet = new Tweet();
		tweet.setText(tweetJson.getString("text"));
		
		JsonObject user = tweetJson.getObject("user");
		tweet.setAuthor(user.getString("name"));
		tweet.setLocation(user.getString("location"));
		
		return tweet;
	}
	
	/**
	 * geocode location as future
	 */
	public static Future<Location> geocodeLocation(String location) {
		String url = "https://maps.googleapis.com/maps/api/geocode/json";
		return new AsyncHttpClient().prepareGet(url)
				.addQueryParam("address", location)
				.execute(new AsyncCompletionHandler<Location>(){

					@Override
					public Location onCompleted(Response res) throws Exception {
						return parseLocation(res.getResponseBody());
					}
				});
	}
	
	/**
	 * geocode a location asynchronously
	 * @param location
	 * @param callback
	 */
	public static void geocodeLocation(String location, Consumer<Location> callback) {
		String url = "https://maps.googleapis.com/maps/api/geocode/json";
		new AsyncHttpClient().prepareGet(url)
				.addQueryParam("address", location)
				.execute(new AsyncCompletionHandler<Location>(){

					@Override
					public Location onCompleted(Response res) throws Exception {
						Location location = parseLocation(res.getResponseBody());
						callback.accept(location);
						return location;
					}
				});
	}
	
	/**
	 * parse Location from Json String
	 * @param locationJson
	 * @return
	 */
	private static Location parseLocation(String locationJson) {
		JsonArray results = Json.parse(locationJson).getArray("results");
		Location latlng = new Location();
		if(results.length() > 0) {
			JsonObject result = results.getObject(0);
			JsonObject geometry = result.getObject("geometry");
			JsonObject location = geometry.getObject("location");
			latlng = new Location(location.getNumber("lat"), location.getNumber("lng"));
			return latlng;
		}
		return latlng;
	}
}
