package de.mo.TwitterMap.model;

public class Tweet {
	private String text;
	
	private String author;
	
	private String location;
	
	private Location geoLocation;
	
	public Tweet() {
	}
	
	public String getText() {
		return text;
	}
	public void setText(String text) {
		this.text = text;
	}

	public String getAuthor() {
		return author;
	}

	public void setAuthor(String author) {
		this.author = author;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}
	
	public String toString() {
		return this.author + ": " + this.text + " from " + this.location;
	}

	public Location getGeoLocation() {
		return geoLocation;
	}

	public void setGeoLocation(Location geoLocation) {
		this.geoLocation = geoLocation;
	}
	
}
