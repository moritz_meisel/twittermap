package de.mo.TwitterMap.presenter;

import java.util.List;

import de.mo.TwitterMap.model.Location;
import de.mo.TwitterMap.model.Tweet;
import de.mo.TwitterMap.model.TwitterService;
import de.mo.TwitterMap.view.SearchMapView;
import rx.Observable;

public class SearchMapPresenter {

	class TweetContainer {
		Tweet tweet;
		Location location;
		
		public TweetContainer(Tweet tweet, Location location) {
			this.tweet = tweet;
			this.location = location;
		}
	}
	
	public SearchMapPresenter(SearchMapView view) {

		view.addSearchClickListener(listener -> {
//			try {
//				String token = TwitterService.getToken().get();
//				List<Tweet> result = TwitterService.searchTweets(token, view.getSearchValue()).get();
//				result.stream().forEach(tweet -> {
//					try {
//						Location location = TwitterService.geocodeLocation(tweet.getLocation()).get();
//						if(location != null) {
//							view.addMarker(location, tweet.getText());
//						}
//					} catch (Exception e) {
//						e.printStackTrace();
//					}
//				});
//			} catch (Exception e) {
//				e.printStackTrace();
//			}
			
			TwitterService.getToken(token -> {
				TwitterService.searchTweets(token, view.getSearchValue(), tweets -> {
					tweets.forEach(tweet -> {
						if(tweet.getLocation() != null) {
							TwitterService.geocodeLocation(tweet.getLocation(), location -> {
								if(location.getLat() != 0) {
									tweet.setGeoLocation(location);
								}
							});
						}
					});
					
				});
			});
			
			
			view.clearMarker();
			Observable.<String>from(TwitterService.getToken())
				.flatMap(token -> Observable.<List<Tweet>>from(TwitterService.searchTweets(token, view.getSearchValue())))
				.flatMapIterable(foundTweets -> foundTweets)
				.filter(tweet -> tweet.getLocation() != null)
				.flatMap(tweet -> {
					return Observable.from(TwitterService.geocodeLocation(tweet.getLocation()))
							.filter(location -> location.getLat() != 0)
							.map(location -> {
								tweet.setGeoLocation(location);
								return tweet;
							});
				}).subscribe(tweet -> view.addMarker(tweet.getGeoLocation(), tweet.getText()));
			
		});
	}

}
