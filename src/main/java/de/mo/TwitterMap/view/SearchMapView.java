package de.mo.TwitterMap.view;

import com.vaadin.annotations.Theme;
import com.vaadin.event.ShortcutAction;
import com.vaadin.event.ShortcutListener;
import com.vaadin.tapio.googlemaps.GoogleMap;
import com.vaadin.tapio.googlemaps.client.LatLon;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.CssLayout;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;

import de.mo.TwitterMap.model.Location;

@Theme("maptheme")
public class SearchMapView extends VerticalLayout {

	private Button searchButton;
	private TextField searchField;
	private GoogleMap googleMap;
	
	public SearchMapView() {
		this.setSizeFull();
		this.addStyleName("tweet-map");
		
		// init toolbar
		CssLayout toolbar = new CssLayout();
		toolbar.addStyleName("toolbar");
        searchField = new TextField();
        searchButton = new Button("Search");
        searchButton.setCaption("Search");
        toolbar.addComponents(searchField, searchButton);
        toolbar.setHeight(50, Unit.PIXELS);
        this.addComponents(toolbar);
        
        // init map
        googleMap = new GoogleMap(null, null, null);
        googleMap.addStyleName("google-map");
        googleMap.setHeight("500px");
        googleMap.setCenter(new LatLon(60.440963, 22.25122));
        googleMap.setZoom(2);
        googleMap.setSizeFull();
        googleMap.setMinZoom(2);
        googleMap.setMaxZoom(16);
        
        this.addComponent(googleMap);
        this.setMargin(true);
        this.setSpacing(true);
        
        // add enter shortcut to textfield
        this.searchField.addShortcutListener(new ShortcutListener("Shortcut Name", ShortcutAction.KeyCode.ENTER, null) {
        	@Override
	    	public void handleAction(Object sender, Object target) {
        		searchButton.click();
        	}
    	});
	}
	
	/**
	 * add marker at location with text
	 * @param location
	 * @param text
	 */
	public void addMarker(Location location, String text) {
		 googleMap.addMarker(text, new LatLon(
				 location.getLat(), location.getLng()), false, null);
	}
	 
	public void addSearchClickListener(ClickListener listener) {
		this.searchButton.addClickListener(listener);
	}
	
	public String getSearchValue() {
		return this.searchField.getValue();
	}

	public void clearMarker() {
		this.googleMap.clearMarkers();
	}
	
}
